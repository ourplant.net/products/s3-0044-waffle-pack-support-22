Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0044-waffle-pack-support-22).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         | [de](https://gitlab.com/ourplant.net/products/s3-0044-waffle-pack-support-22/-/raw/main/01_operating_manual/S3-0044_A3_Betriebsanleitung.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0044-waffle-pack-support-22/-/raw/main/01_operating_manual/S3-0044_A1_Operating%20Manual.pdf)                   |
| assembly drawing         |[de](https://gitlab.com/ourplant.net/products/s3-0044-waffle-pack-support-22/-/raw/main/02_assembly_drawing/s3-0044_A_ZNB_waffle_pack_support_22.pdf)                  |
| circuit diagram          |                  |
| maintenance instructions | [de](https://gitlab.com/ourplant.net/products/s3-0044-waffle-pack-support-22/-/raw/main/04_maintenance_instructions/S3-0011_B_WA_WPS.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0044-waffle-pack-support-22/-/raw/main/04_maintenance_instructions/S3-0011_B_MI_WPS.pdf)                   |
| spare parts              |                  |

